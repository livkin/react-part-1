
import React from 'react';

export default class HeaderMessagesCount extends React.Component {

    constructor() {
        super();
    }

    render() {

        return (

            <div className="header">
                <div className="header-title">
                    {this.props.headerTitle}
                </div>
                <div className="header-users-count">
                    Count of users: {this.props.headerUsersCount}
                </div>
                <div className="header-messages-count">
                    Count of messages: {this.props.headerMessagesCount}
                </div>
                <div className="header-last-message-date">
                    {this.props.headerLastMessageDate}
                </div>
            </div>

        )


    }
}