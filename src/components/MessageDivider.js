import React from 'react';

export default class MessageDivider extends React.Component{

  constructor() {
    super();
  }

  render () {
    return (
      <div className = "message-divider">
        ----------{this.props.day}------------
      </div>
    );
  }

}