import React from 'react';

export default class OwnMessage extends React.Component{

  constructor() {
    super();
  }

  render () {
    return (
      <div className = "own-message">
        <div>"Own message"</div>
        <div className = "message-text">{this.props.text}</div>
        <div className = "message-time">{this.props.time}</div>
        <button className = "message-edit">{this.props.edit}</button>
        <button className = "message-delete">{this.props.delete}</button>
      
      </div>
    );
  }

}