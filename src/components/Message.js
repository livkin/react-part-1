import React from 'react';
import Avatar from './message/Avatar';

export default class Message extends React.Component{

  constructor() {
    super();
  }

  render () {
    return (
      <li className = "message" key = {this.props.index}>
        <div>==============================</div>
        <div>"Message"</div>
        <Avatar src =  {this.props.userAvatarSrc} />
        <div className = "message-user-name">{this.props.userName}</div>
        <div className = "message-time">{this.props.time}</div>
        <div className = "message-text">{this.props.text}</div>
        <div className = "message-liked">{this.props.liked}</div>
        <button className = "message-like">{this.props.like}</button>
      </li>
    );
  }

}