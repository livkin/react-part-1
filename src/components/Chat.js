import React from 'react';
import Header from "./Header";
import MessageList from './MessageList';
import MessageInput from './MessageInput';


export default class Chat extends React.Component {

    constructor() {
        super();
        this.state = { chat: [], isFetching: false };
    }

    componentDidMount() {
        this.fetchChat(this.props.url);
    }

    fetchChat = (url) => {
        this.setState({...this.state, isFetching: true});
        fetch(url)
            .then(response => response.json())
            .then(result => {
                this.setState({chat: result, isFetching: false})
            })
            .catch(e => {
                console.log(e);
                this.setState({...this.state, isFetching: false});
            });
    };

    render() {
        return (
            
            <div className="chat">
                <Header
                    headerTitle="Chat title"
                    headerUsersCount="12"
                    headerMessagesCount={this.state.chat.length}
                    headerLastMessageDate="01/01/01"
                />
                <MessageList data = {this.state.chat} />
                <MessageInput />
            </div>
        )
    }
}