import React from 'react';

export default class Avatar extends React.Component {

  constructor() {
    super();
  }

  render() {
    return (
      <div className = "message-user-avatar">
        <img src = { this.props.src }></img>
      </div>
    );
  }

}