import React from 'react';
import MessageDivider from './MessageDivider';
import Message from './Message';
import OwnMessage from './OwnMessage';


export default class MessageList extends React.Component {

  getMessageList(data) {

    let currDay = new Date();

    return data.map(((item, index) => {

      return (
        <Message
          index={index}
          userAvatarSrc={item.avatar}
          userName={item.user}
          time={item.createdAt}
          text={item.text}
          liked="YES"
          like="Like"
        />
      )

      // const res = this.getMessage(item, index, currDay);

      // currDay = new Date(Date.parse(item.createdAt));

      // return res;

    }))
  }

  getMessage(item, index, currDay) {

    let dividerText = "";

    if (!this.theSameDay(currDay, new Date(Date.parse(item.createdAt)))) {
      if (this.theSameDay(new Date(Date.parse(item.createdAt)), new Date())) {
        dividerText = "Today";
      } else {
        dividerText = new Date(Date.parse(item.createdAt)).toLocaleDateString;
      }
    }

    currDay = new Date(Date.parse(item.createdAt));

    return (

      <Message
        index={index}
        userAvatarSrc={item.avatar}
        userName={item.user}
        time={item.createdAt}
        text={item.text}
        liked="YES"
        like="Like"
      />


    )

  }

  theSameDay(first, second) {
    return first.getFullYear() === second.getFullYear() &&
      first.getMonth() === second.getMonth() &&
      first.getDate() === second.getDate();
  }

  render() {
    return (
      <ul className="message-list">

        {this.getMessageList(this.props.data)}

      </ul>
    );
  }

}