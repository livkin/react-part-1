import React from 'react';

export default class MessageInput extends React.Component{

  constructor() {
    super();
  }

  render () {
    return (
      <div className = "message-input">
        <div>"Message input"</div>
        <input className = "message-input-text"></input>
        <button className = "message-input-button">Send</button>
      </div>
    );
  }

}