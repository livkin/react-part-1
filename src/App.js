import Chat from './components/Chat'
import './App.css';

const URL = 'https://edikdolynskyi.github.io/react_sources/messages.json';

function App() {
  return (
    <div className="App">
      <Chat url = {URL}/>
    </div>
  );
}

export default App;
